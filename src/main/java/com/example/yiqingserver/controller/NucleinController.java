package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Nuclein;
import com.example.yiqingserver.mapper.NucleinMapper;
import com.example.yiqingserver.service.NucleinService;
import com.example.yiqingserver.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("sys-nuclein")
public class NucleinController {

    @Resource
    private NucleinService nucleinService;

    @Resource
    private NucleinMapper nucleinMapper;

    /**
     * 填写核酸信息，上传给学院管理员-------------已测试
     * @param nuclein
     * @return
     */
    @PostMapping("uploadToAcademy")
    public Result uploadToAcademy(@RequestBody Nuclein nuclein){
        return nucleinService.uploadToAcademy(nuclein);
    }

    /**
     * 根据用户uid查询核酸上传历史信息，用于展示在用户个人页面
     * @param uid
     * @return
     */
    @GetMapping("getNucleinById")
    public Result getNucleinById(Integer uid){
//        QueryWrapper wrapper = new QueryWrapper<>();
//        wrapper.eq("u_id",uid);
//        final List<Nuclein> list = nucleinMapper.selectList(wrapper);
//        return new Result<>(200,"查询个人核酸上传历史信息成功",list);
        final List<Nuclein> nucleinById = nucleinMapper.getNucleinById(uid);
        return new Result<>(200,"这是用户展示在个人主页的核酸信息",nucleinById);
    }
    /**
     * 根据所属学院或部门查询对应的人员的核酸信息
     */
    @GetMapping("getAcademyNuc")
    public Result getAcademyNuc(String academy){
//        QueryWrapper wrapper = new QueryWrapper<Nuclein>();
//        wrapper.eq("academy",academy);
//        final List list = nucleinMapper.selectList(wrapper);
//        return new Result<>(200,"这是各自学院/部门的核酸信息",list);
        final List<Nuclein> academyNuc = nucleinMapper.getAcademyNuc(academy);
        return new Result<>(200,"这是展示给学院的核酸信息",academyNuc);
    }

    /**
     * 学院上传核酸信息给学校，状态status 0->1
     * @param idList
     * @return
     */
    @PostMapping("uploadToSchool")
    public Result uploadToSchool(Integer idList[]){
        System.out.println(idList);
        for(Integer id : idList){
            nucleinMapper.uploadToSchool(id);
        }
        return new Result<>(200,"上传成功",idList);
    }

    /**
     * 根据状态查询核酸信息，用于学校展示核酸数据
     * @return
     */
    @GetMapping("getNucleinByStatus")
    public Result getNucleinByStatus(){
//        QueryWrapper wrapper = new QueryWrapper<Nuclein>();
//        wrapper.eq("status",true);
//        final List list = nucleinMapper.selectList(wrapper);
//        return new Result<>(200,"这是展示给学校管理员的核酸数据",list);
        final List<Nuclein> nucleinByStatus = nucleinMapper.getNucleinByStatus();
        return new Result<>(200,"这是展示给学校管理员的核酸数据",nucleinByStatus);
    }
    /**
     * 删除单条核酸数据----学校+学院/部门
     */
    @DeleteMapping("delete/{id}")
    public Result delNuclein(Integer id){
        QueryWrapper wrapper = new QueryWrapper<Nuclein>();
        wrapper.eq("id",id);
        nucleinMapper.delete(wrapper);
        return new Result<>(200,"删除记录"+id+"成功",null);
    }
}
