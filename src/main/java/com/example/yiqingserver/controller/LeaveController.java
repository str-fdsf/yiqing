package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Leave;
import com.example.yiqingserver.mapper.LeaveMapper;
import com.example.yiqingserver.service.LeaveService;
import com.example.yiqingserver.utils.Result;
import com.mysql.cj.protocol.ResultsetRow;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("sys-leave")
public class LeaveController {

    @Resource
    private LeaveService leaveService;
    @Resource
    private LeaveMapper leaveMapper;

    /**
     * 出入登记
     *
     * @param leave
     * @return
     */
    @PostMapping("entry")
    public Result entry(@RequestBody Leave leave) {
        return leaveService.entry(leave);
    }

    /**
     * 出入信息统计
     */
    @GetMapping("leaveCensus")
    public Result leaveCensus() {
        return leaveService.leaveCensus();
    }

    /**
     * 审核
     */
    @PostMapping("audit")
    public Result audit(Integer id) {
        if (id == 1) {
            leaveMapper.pass(id);
            return new Result<>(200, "审核通过", null);
        } else {
            leaveMapper.fail(id);
            return new Result<>(401,"审核不通过",null);
        }
    }
    /**
     * 删除出入信息记录——学校+保卫科
     */
    @DeleteMapping("delete/{id}")
    public Result delLeave(Integer id){
        QueryWrapper wrapper = new QueryWrapper<Leave>();
        wrapper.eq("id",id);
        leaveMapper.delete(wrapper);
        return new Result<>(200,"删除记录"+id+"成功",null);
    }

    /**
     * 修改出入信息
     * @param leave
     * @return
     */
    @PostMapping("updateLeave")
    public Result updateLeave(@RequestBody Leave leave){
        return  leaveService.updateLeave(leave);
    }
}
