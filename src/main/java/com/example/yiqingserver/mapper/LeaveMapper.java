package com.example.yiqingserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yiqingserver.entity.Leave;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface LeaveMapper extends BaseMapper<Leave> {

    public void pass(Integer id);

    public void fail(Integer id);

    public void updateLeave(Leave leave);
}
