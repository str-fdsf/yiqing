package com.example.yiqingserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yiqingserver.entity.Notice;
import com.example.yiqingserver.req.UpdateNoticeReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface NoticeMapper extends BaseMapper<Notice> {

    public void updateNotice(UpdateNoticeReq updateNoticeReq);
}
