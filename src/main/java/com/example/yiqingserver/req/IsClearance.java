package com.example.yiqingserver.req;
import lombok.Data;
@Data
public class IsClearance {

    private Integer id;

    private boolean status;
}
