package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_notice")
public class Notice {

    @TableId(type = IdType.AUTO)
    private Integer id;
//   通知标题
    private String title;
//    发布时间
    private String publishTime;
//    通知内容
    private String content;
//    发布人(姓名)
    private String createMan;

}
