package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_nuclein")
public class Nuclein {

    @TableId(type = IdType.AUTO)
    private Integer id;
    //    检测时间
    private String time;
    //    类型 1.阳性 2.阴性
    private String type;
    //    上传状态 1.未上传（0） 2.已上传（1）
    private boolean status;
    //    核酸码，图片文件
    private String code;
    //    关联user表中的u_id;
    private Integer uId;
    //    备注
    private String remarks;

    //    用户
    private User userInfo;


}
