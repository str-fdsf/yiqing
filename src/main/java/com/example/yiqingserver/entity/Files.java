package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_files")
public class Files {
    @TableId(value = "file_id",type = IdType.AUTO)
    private Integer fileId;
    @TableField(value = "file_name")
    private String fileName;
    @TableField(value = "file_type")
    private String fileType;
    @TableField(value = "file_size")
    private Long fileSize;
    @TableField(value = "file_url")
    private String fileUrl;
    @TableField(value = "file_md5")
    private String fileMd5;
    @TableField(value = "is_delete")
    private boolean isDelete;
    @TableField(value = "enable")
    private boolean enable;
}
