package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_leave")
public class Leave {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer uId;
    //    真实姓名
    private String name;
    //    类型 1.出 2.入
    private String type;
    //    审核状态 1.待审核（0）3.通过（1）4.未通过（2）
    private Integer status;
    //    出入原因
    private String reason;
    //    出/入校时间
    private String leaveTime;
    //    证件信息
    private String credential;
}
