package com.example.yiqingserver.utils;

import java.util.Calendar;

public class TimeUtils {

//    返回当前时间
    public String backTime(){
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        String date = year + "-" + month + "-" + day;
        return date;
    }
}
