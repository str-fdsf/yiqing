package com.example.yiqingserver.service;

import com.example.yiqingserver.entity.Area;
import com.example.yiqingserver.utils.Result;

public interface AreaService {

    public Result areaCensus();

    public Result addControlArea(Area area);
}
