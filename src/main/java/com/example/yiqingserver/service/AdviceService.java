package com.example.yiqingserver.service;

import com.example.yiqingserver.entity.Advice;
import com.example.yiqingserver.utils.Result;

public interface AdviceService {

    public Result submitAdvice(Advice advice);
    public Result adCensus();

    public Result typeCensus();
}
