package com.example.yiqingserver.service;

import com.example.yiqingserver.entity.Nuclein;
import com.example.yiqingserver.utils.Result;

import javax.annotation.Resource;

public interface NucleinService {

    public Result uploadToAcademy(Nuclein nuclein);
}
