package com.example.yiqingserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Area;
import com.example.yiqingserver.mapper.AreaMapper;
import com.example.yiqingserver.service.AreaService;
import com.example.yiqingserver.utils.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class AreaServiceImpl implements AreaService {

    @Resource
    private AreaMapper areaMapper;
    @Override
    public Result areaCensus() {
        QueryWrapper wrapper1 = new QueryWrapper<Area>();
        wrapper1.eq("area_type","宿舍区域");
        QueryWrapper wrapper2 = new QueryWrapper<Area>();
        wrapper2.eq("area_type","教学区域");
        QueryWrapper wrapper3 = new QueryWrapper<Area>();
        wrapper3.eq("area_type","办公区域");
        QueryWrapper wrapper4 = new QueryWrapper<Area>();
        wrapper4.eq("area_type","生活区域");
//        宿舍区域
        final Long dormArea = areaMapper.selectCount(wrapper1);
//        教学区域
        final Long techArea = areaMapper.selectCount(wrapper2);
//        办公区域
        final Long officeArea = areaMapper.selectCount(wrapper3);
//        生活区域
        final Long lifeArea = areaMapper.selectCount(wrapper4);
        Map<String,Long> map = new HashMap<>();
        map.put("宿舍区域",dormArea);
        map.put("教学区域",techArea);
        map.put("办公区域",officeArea);
        map.put("生活区域",lifeArea);
        return new Result<>(200,"这是区域类型统计",map);
    }

    @Override
    public Result addControlArea(Area area) {
        area.setStatus(true);
        areaMapper.insert(area);
        return new Result(200,"添加成功",area);
    }
}
