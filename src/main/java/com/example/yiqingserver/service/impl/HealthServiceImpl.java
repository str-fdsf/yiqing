package com.example.yiqingserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Health;
import com.example.yiqingserver.mapper.HealthMapper;
import com.example.yiqingserver.req.HealthReq;
import com.example.yiqingserver.service.HealthService;
import com.example.yiqingserver.utils.Result;
import com.example.yiqingserver.utils.TimeUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class HealthServiceImpl implements HealthService {

    @Resource
    private HealthMapper healthMapper;
    @Override
    public Result clockIn(Health health) {
        QueryWrapper wrapper = new QueryWrapper<>();
        TimeUtils time = new TimeUtils();
        String date = time.backTime();
        health.setCreateTime(date);
        wrapper.eq("u_id",health.getUId());
        wrapper.eq("create_time",health.getCreateTime());
        final Health health1 = healthMapper.selectOne(wrapper);
        if(!ObjectUtils.isEmpty(health1)){
            return new Result<>(401,"今日已经打过卡了,明日再来",null);
        }else {
            health.setStatus(false);
            healthMapper.insert(health);
            return new Result<>(200,"打卡成功",health);
        }
    }
}
