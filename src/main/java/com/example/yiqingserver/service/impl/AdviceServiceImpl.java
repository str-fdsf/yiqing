package com.example.yiqingserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Advice;
import com.example.yiqingserver.mapper.AdviceMapper;
import com.example.yiqingserver.service.AdviceService;
import com.example.yiqingserver.utils.Result;
import com.example.yiqingserver.utils.TimeUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdviceServiceImpl implements AdviceService {

    @Resource
    private AdviceMapper adviceMapper;

    @Override
    public Result submitAdvice(Advice advice) {
        TimeUtils timeUtils = new TimeUtils();
        advice.setSubmitTime(timeUtils.backTime());
        advice.setStatus(0);
        adviceMapper.insert(advice);
        return new Result<>(200, "提交成功，等待学校受理", advice);
    }

    @Override
    public Result adCensus() {
        QueryWrapper wrapper1 = new QueryWrapper<Advice>();
        wrapper1.eq("status", 0);
        QueryWrapper wrapper2 = new QueryWrapper<Advice>();
        wrapper2.eq("status", 1);
        QueryWrapper wrapper3 = new QueryWrapper<Advice>();
        wrapper3.eq("status", 2);
//        总数
        final Long total = adviceMapper.selectCount(null);
//        待处理数
        final Long todoCount = adviceMapper.selectCount(wrapper1);
//        处理中数
        final Long doingCount = adviceMapper.selectCount(wrapper2);
//        已处理数
        final Long doneCount = adviceMapper.selectCount(wrapper3);
//        将数据放入数组中
        Map<String, Long> map = new HashMap<>();
        map.put("意见处理总数", total);
        map.put("待处理意见", todoCount);
        map.put("处理中意见", doingCount);
        map.put("已处理意见", doneCount);
        return new Result<>(200, "这是意见统计", map);
    }

    @Override
    public Result typeCensus() {
        QueryWrapper wrapper1 = new QueryWrapper<Advice>();
        wrapper1.eq("type","系统意见");
        QueryWrapper wrapper2 = new QueryWrapper<Advice>();
        wrapper2.eq("type","物资意见");
        QueryWrapper wrapper3 = new QueryWrapper<Advice>();
        wrapper3.eq("type","封控意见");
        QueryWrapper wrapper4 = new QueryWrapper<Advice>();
        wrapper4.eq("type","其它意见");

//        系统意见
        final Long sysAdvice = adviceMapper.selectCount(wrapper1);
//        物资意见
        final Long goodsAdvice = adviceMapper.selectCount(wrapper2);
//        封控意见
        final Long closeAdvice = adviceMapper.selectCount(wrapper3);
//        其它意见
        final Long otherAdvice = adviceMapper.selectCount(wrapper4);
        Map<String,Long> map = new HashMap<>();
        map.put("系统意见",sysAdvice);
        map.put("物资意见",goodsAdvice);
        map.put("封控意见",closeAdvice);
        map.put("其它意见",otherAdvice);
        return new Result<>(200,"这是意见类型统计数据",map);
    }
}
