package com.example.yiqingserver;

import com.example.yiqingserver.entity.User;
import com.example.yiqingserver.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class YiqingServerApplicationTests {

    @Resource
    private UserMapper userMapper;
    @Test
    void contextLoads() {
        final List<User> users = userMapper.selectList(null);
        System.out.println(users);

    }

}
